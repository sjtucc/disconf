package com.xyzq; /**
 * @Title: ${file_name}
 * @Description:
 * @author chenchen
 * @date 2019/7/49:55
 */

import com.baidu.disconf.client.common.update.IDisconfUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @author chenchen
 * @date 2019/7/4 9:55
 */
@RestController
public class Test {
    @Autowired
    JedisConfig j;

    @Autowired
    ServiceA serviceA;

    @RequestMapping("/test")
    public String test() {
        System.out.println("---------------YYYY-----------------");
        System.out.println(j.getHost());
        System.out.println(j.getPort());
        System.out.println("--------------YYYY-------------------");
        System.out.println(serviceA.getVarAA());
        System.out.println(serviceA.getVarBB());
        return "44444";
    }

}
