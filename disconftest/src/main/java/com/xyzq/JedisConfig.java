package com.xyzq; /**
 * @Title: ${file_name}
 * @Description:
 * @author chenchen
 * @date 2019/7/49:42
 */

/**
 * @Description:
 * @author chenchen
 * @date 2019/7/4 9:42
 */
import com.baidu.disconf.client.common.annotations.DisconfFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import com.baidu.disconf.client.common.annotations.DisconfFileItem;
import com.baidu.disconf.client.common.update.IDisconfUpdate;

@Configuration
@DisconfFile(filename = "redis.properties")
public class JedisConfig {

    protected static final Logger LOGGER = LoggerFactory
            .getLogger(JedisConfig.class);

    // 代表连接地址
    private String host;

    // 代表连接port
    private int port;

    /**
     * 地址, 分布式文件配置
     *
     * @return
     */
    @DisconfFileItem(name = "redis.host", associateField = "host")
    public String getHost() {

        return host;
    }

    public void setHost(String host) {

        this.host = host;
    }

    /**
     * 端口, 分布式文件配置
     *
     * @return
     */
    @DisconfFileItem(name = "redis.port", associateField = "port")
    public int getPort() {

        return port;
    }

    public void setPort(int port) {

        this.port = port;
    }

}