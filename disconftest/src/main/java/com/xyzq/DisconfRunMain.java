package com.xyzq; /**
 * @Title: ${file_name}
 * @Description:
 * @author chenchen
 * @date 2019/7/49:44
 */

/**
 * @Description:
 * @author chenchen
 * @date 2019/7/4 9:44
 */

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class DisconfRunMain {

    @Autowired
    JedisConfig jedisConfig;

    static JedisConfig j;

    @PostConstruct
    public void init(){
        j = jedisConfig;
    }
    public static void main(String args[]) {

        SpringApplication.run(DisconfRunMain.class, args);
        System.out.println("--------------------------------");
        System.out.println(j.getHost());
        System.out.println(j.getPort());

    }

}
