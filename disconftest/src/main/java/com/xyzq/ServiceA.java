package com.xyzq; /**
 * @Title: ${file_name}
 * @Description:
 * @author chenchen
 * @date 2019/7/412:13
 */

import com.baidu.disconf.client.common.annotations.DisconfItem;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @author chenchen
 * @date 2019/7/4 12:13
 */
@Component
public class ServiceA {

    private String varAA;
    private String varBB;

    @DisconfItem(key = "chenkey")
    public String getVarAA() {
        return varAA;
    }

    public void setVarAA(String varAA) {
        this.varAA = varAA;
    }

    @DisconfItem(key = "key2")
    public String getVarBB() {
        return varBB;
    }

    public void setVarBB(String varBB) {
        this.varBB = varBB;
    }
}
