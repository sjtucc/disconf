package com.xyzq; /**
 * @Title: ${file_name}
 * @Description:
 * @author chenchen
 * @date 2019/7/412:20
 */

import com.baidu.disconf.client.common.annotations.DisconfUpdateService;
import com.baidu.disconf.client.common.update.IDisconfUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @author chenchen
 * @date 2019/7/4 12:20
 */
@Service
@DisconfUpdateService(classes = { ServiceA.class}, itemKeys = {"chenkey", "key2"})
public class ServiceACallback implements IDisconfUpdate {

    @Autowired
    ServiceA serviceA;

    @Override
    public void reload() throws Exception {
        System.out.println("----------------PPPPP---------");
        System.out.println(serviceA.getVarAA());
        System.out.println(serviceA.getVarBB());
    }
}
