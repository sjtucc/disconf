package com.xyzq; /**
 * @Title: ${file_name}
 * @Description:
 * @author chenchen
 * @date 2019/7/410:38
 */

import com.baidu.disconf.client.common.annotations.DisconfUpdateService;
import com.baidu.disconf.client.common.update.IDisconfUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description:
 * @author chenchen
 * @date 2019/7/4 10:38
 */
@Service
@DisconfUpdateService(classes = { JedisConfig.class})
public class DisConfigUpdateCallback implements IDisconfUpdate {
    @Autowired
    private JedisConfig jedisConfig;

    @Override
    public void reload() throws Exception {
        System.out.println("--------------XXX----------------");
        System.out.println(jedisConfig.getHost());
    }
}
